
const express = require('express');
const { application } = require('express');
const app = express();
app.use(express.json());



// Joi is a Class that allows us define objects and values inside our routes
// console.log(Joi)

const persons = [
    {id: 1, name: 'Tom'},
    {id: 2, name: 'Kris' }
]

// GET first massages
app.get('/', (req, res)=>{
    res.send("First test message ")
    });

// GET all persons
app.get('/api/persons', (req, res)=>{
    res.send(persons);
});

// GET desired person
app.get('/api/persons/:id',(req, res)=>{
    const person = persons.find(p => p.id === parseInt(req.params.id))
    if(!person)
        res.status(404).send("There's any person that matches that id");
        res.send(person) 
})

// see what you will get when insted of year you type 2022 and month 12 day 31 in this endpoint
app.get('/api/posted/:year/:month/:day',(req, res)=>{
res.send(req.params)
})
// to commit empty massage use: git commit -a --allow-empty-message -m ''
const PORT = 3000 || process.env;   

// what's process.env ? To read whole object use below:
// let ProcessENV = process.env;
// console.log(ProcessENV);
 
// 
// let ProcessENV2 = process.env.USERNAME;
// console.log(ProcessENV2);

// WE want to POST some person

app.post('/api/persons',(req, res)=>{

    if(!req.body.name || req.body.name.length < 2){
        res.status(400).send("name must have at least 2 characters");
        return;
    }

    const pe = {
        id: persons.length +1,
        name: req.body.name,
    };

    persons.push(pe);
    res.send(pe);

})

// We want to PUT some person

app.put('/api/persons/:id', (req, res)=>{

// // If course doesn't exist display error(404)
//     const person = persons.find(p => p.id === parseInt(req.params.id))
//     if(!person)
//         res.status(404).send("There's any person that matches that id");
    

// update
persons.name = req.body.name;
res.send(persons);


})

app.listen(PORT, ()=> console.log(`http://localhost:${PORT}`));

