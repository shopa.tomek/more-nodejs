const fs = require('fs');

// It's better to use async method
// sync or blocking
// this sync method and show's all the files in "more-nodejs" folder
 const files = fs.readdirSync('./');
 console.log(`In this example method used is sync: ${files}`);

// async or non-blocking
fs.readdir('./',function(err,files){
    if (err) 
    console.log('Error', err);
    else console.log('In this example method is async:', files )
})

// For example access is an async method:
// const access = fs.access();

// console.log(access);
console.log(" file system works");
module.exports.fs = fs;