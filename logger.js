
let url = 'http://logger.io/log';
const EventEmitter = require('events');


 
// extends means we addding a parent a base class

class Logger extends EventEmitter{
    log(message){
        console.log(message);
        //  we are raising an emitter
    this.emit('logged', {id: 1, url: 'http://google.com'});
     
    }   
}


// We making our module public (visible)
// You can export function or object
module.exports = Logger;


// 6-module wrapper function 

// every module has "non visible" function wrapper like this below:
// (function (exports, require, module, __filename, __dirname) {});
// you can console.log each of this elements form parameter. ex:
console.log(__dirname);
console.log(__filename);
console.log(require);
console.log(module);
console.log(exports);

// 7- path module

// if you want to see nodejs go to https://nodejs.org/dist/latest-v16.x/docs/api/

// 8- Os 

// 9 - filesystem

// 10 - events module / core functionalities are based on event's

