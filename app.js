 const log = require('./logger');
 const path = require('./path');
 const fs = require('./fs');
 const events = require('./events');
 const { appendFile, fstat } = require("fs");


console.log(log);
log("message I sent");

// What modules are ?


// seTimeout(), setInterval(), clearInterval()  - belongs to window object
// they can be write as for example: window.console.log, window.seTimeout
// In node these object are belong to object called "global"
// So we can type global.console.log, global.seTimeout

// modules are small building blocks where we define our variables and functions

// there is a main module for example "app.js" and modules added extra to it
// console.log(module);

// npm list --depth=0 // show dependencies only in this application
// npm view express // shows all info about express
// npm view express dependencies // this command shows all dependencies of express framework/module
// npm view express versions // shows all versions of express (past and present)

// how downgrade node package?? // npm i express@x.x.x // install express version x.x.x
 
// how to check outdated (old) packages ? // npm outdated

//  update packages // npm update
// how to update packages to the latest versions ? // sudo npm i -g npm-check-

// how to install package with dev dependencies ? // npm install <package-name> --save-dev

// to unistall package // npm unistall or un <package-name>

// local packages are installed in the directory where you run npm install <package-name>, and they are put in the node_modules folder under this directory
// global packages are all put in a single place in your system (exactly where depends on your setup), regardless of where you run npm install -g <package-name>

 