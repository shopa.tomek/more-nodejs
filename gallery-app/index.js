const express = require('express');
const app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var morgan = require('morgan');
const fs = require('fs');
var path = require('path');
require('dotenv/config');
var multer = require('multer');
var exhbs = require('express-handlebars');
const imageThumbnail = require('image-thumbnail');
console.log(exhbs);

app.engine('.hbs', exhbs.engine({ defaultLayout: "main", extname: "hbs" }))
app.set('view engine', '.hbs')
app.use(express.static('./public'))
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


// Multer

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './uploads')
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname)
    }
})
var upload = multer({ storage: storage })



// UPLOAD FILES POST

app.post('/', upload.array('profile-files', 12), function (req, res, next) {
    // req.files is array of `profile-files` files
    // req.body will contain the text fields, if there were any
    response = "File uploaded successfully</br>"
  
    for(var i=0;i<req.files.length;i++){
         response += `<img src="${req.files[i].path}"  id="frame" src="" width="200px" height="200px"/><br>`
    }
    
    return res.send(response)
})
   
// thumbnail

// https://www.npmjs.com/package/image-thumbnail

// 

let filenames = fs.readdirSync('uploads');

filenames.forEach((file) => {
 
  
    console.log("File:", file);
});
console.log(filenames)


// Home page TESTING
let why = "<b>This the simple handlebars expression example</b>"
app.get('/', (req, res) => {
    res.render("home", { text: why },)
})

 app.get('/test', (req, res)=>{
     res.render("test", { text: "test"})
});




const PORT = 5000

app.listen(PORT, () => {
    console.log(`Gallery app link: http://localhost:${PORT}`)
})


// https://www.geeksforgeeks.org/upload-and-retrieve-image-on-mongodb-using-mongoose/
// handlebars docs
// https://www.npmjs.com/package/express-handlebars
// upload images using handlebars
// {{!-- https://stackoverflow.com/questions/58190819/upload-image-on-form-in-express-handlebars --}}
// https://www.geekstrick.com/create-website-with-handlebars-express-nodejs/
// https://www.youtube.com/watch?v=HxJzZ7fmUDQ&ab_channel=WaelYasmina





