const path = require('path');
const os = require('os');

const totalmem = os.totalmem();
const freeMem = os.freemem(); 

console.log(freeMem);
console.log(os.platform());
console.log(os.hostname());

// console.log(path);

let pathObj = path.parse(__filename); 
console.log(pathObj);
module.exports.path = path;
module.exports.os = os;