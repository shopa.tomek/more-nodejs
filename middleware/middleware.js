// What's middleware ?
// Technically routes handlers are middleware. Example below;
// app.get(req, res)=> {} 

// request goes through pipeline called: "request proccessing pipeline"
// request ->  middlewares..... -> response or
// request -> json()-> route() -> response

// SO express itself is more or less middleware function

const config = require('config');
const morgan = require('morgan')
const logger = require('./logg')
const express = require('express');
const { urlencoded } = require('express');
const app = express();

app.engine('pug', require('pug').__express);
app.set('view engine', 'pug');
app.set('views', '../views');

app.use(express.json()); 
app.use(urlencoded({ extended: true }));
app.use(express.static('public'));
app.use(morgan('dev'));


// logg.js contains custom middleware functions!
// without using next() after building your custom middleware function your server will be loading forever!

// next () invokes next middleware in the middleware pipleline or middleware stock


// cannot set headers ERROR

// Typical error's:

// 1. The "Cannot set headers after they are sent to the client" error occurs when the server in an express. js application sends more than one response for a single request, e.g. calling res. json() twice. To solve the error make sure to only send a single response for each request.
// ------------------------------------------------------------

// Configuration:

console.log(`App name: ${config.get('name')}`)


console.log(`environment variable: ${process.env.NODE_ENV}`);
// app turning development as default
console.log(`app: ${app.get('env')}`);

// test route
app.get('/',(req, res, next)=>{
    console.log("This is first test route. It's first middleware in the middleware stack!")
    res.send("first get test")
    next();
});

app.use(logger);

// pug render()

app.get('/pug', (req, res)=>{
    res.render('../views', {
        title: 'More nodejs app',
        message: 'first message using pug'
    });
})




// Third party middleware's below:

// https://expressjs.com/en/resources/middleware

// How to create custom middleware function ?
 




const PORT = 4000 || process.env.PORT ;
app.listen(PORT, ()=>console.log(`link http://localhost:${PORT}`));
  